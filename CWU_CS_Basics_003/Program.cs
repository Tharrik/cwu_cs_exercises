﻿/*
    Copyright(C) 2020 Pablo del Fresno Herena

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see<https://www.gnu.org/licenses/>.
*/

/* Exercise Basics 003
 * Create a program that gets two integer numbers from the user and shows
 * the decimal quotient of the division.
 * - You can only code inside the Main function
 * - You can use Console.WriteLine("text") to write in the console.
 * - You can use the IO.CaptureNumber() funtion to read numbers from the user.
 */

using System;
using CWU_CommonLib;

namespace CWU_CS_Basics_003
{
    class Program
    {
        static void Main(string[] args)
        {
            // Your code here
        }
    }
}
